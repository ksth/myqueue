=======
MyQueue
=======

Frontend for SLURM_ and PBS_.

* Documentation: https://myqueue.readthedocs.io/
* Code: https://gitlab.com/myqueue/myqueue/

.. image:: https://gitlab.com/myqueue/myqueue/badges/master/coverage.svg

.. _SLURM: https://slurm.schedmd.com/
.. _PBS: http://www.pbspro.org/
