============
Installation
============

Install with ``pip``::

    $ python3 -m pip install myqueue

.. note::

    Python 3.6 or later is required.

Enable bash tab-completion for future terminal sessions like this::

    $ mq completion >> ~/.profile

Run the tests::

    $ mq test

and report any errors you get: https://gitlab.com/myqueue/myqueue/issues.

Subscribe here_ if you want to be notified of updates on PyPI_.

.. _here: https://libraries.io/pypi/myqueue
.. _PyPI: https://pypi.org/project/myqueue/
